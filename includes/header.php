<?php
if (isset($_GET['protokol']))
{
	echo '<div class="header__notif text-center notif-blue">
	<span>Protokol kesehatan merupakan hal yang penting, untuk itu kami menyiapkan yang terbaik untuk anda.  <a href="protokol.php" class="message">Lihat Protokol Kesehatan Kami</a></span>
	<a href="#" class="close-notif">+</a> 
	</div>';}
else
{
	echo
	'<div class="header__notif text-center">
		<span><b>Dear Customer,</b> Memberitahukan bahwa jam operasional Trans Snow World pada hari Minggu, tanggal 13 Oktober 2019 dibuka pada pukul 13.00 WIB. Untuk hari biasa, jam operasional Trans Studio Cibubur dibuka normal pada pukul 09.00 WIB.</span>
		<a href="#" class="close-notif">+</a> 
	</div>';

}
?>

<header id="header">
	<nav>
		<div class="container">
			<a href="index.php" class="logo">
				<picture>
					<img src="images/logo-tswjuanda@2x.png"
					 alt="Logo Trans Snow World">
<!-- 					<img src="images/logo-tswjuanda@2x.png"
					srcset="
					images/logo-tsw.png 1x,
					images/logo-tsw@2x.png 2x,
					"  alt="Logo Trans Snow World">
 -->				</picture>
			</a>
			
			<ul class="list_menu">
				<li><a href="index.php" class="selected">Home</a></li>
				<li><a href="tiket.php" class="">Tiket</a></li>
				<li><a href="fasilitas.php" class="">Fasilitas</a></li>
				<li><a href="berita.php" class="">Berita</a></li>
				<li><a href="bantuan.php" class="">Bantuan</a></li>
				<li class="drop-parent">
					<a href="index.php#kontakkami" class="">Tentang Kami</a>
					<div class="dropmenu">
						<a href="about.php">Trans Snow World Juanda</a>
						<a href="about.php">Trans Snow World Bintaro</a>
					</div>
				</li>
				<li><a href="protokol.php" class="">Protokol Kesehatan</a></li>
				<li><a href="tiket.php" class="btn_pesan">Pesan Tiket</a></li>
			</ul>

			<ul class="list_signin">				
				<?php
					if (isset($_GET['sign_in'])) { ?>
					    <li class="sign_in">
					    	<a href="#">Halo, Fahrian</a>
					    	<a href="#">Daftar Pembelian</a>
					    	<a href="#">Dashboard detikconnect</a>
					    	<a href="#">Keluar</a>
					    </li>
				<?php } else { ?>
					<li><a alt="../../assets/detikid/login.php?transsnow" class="box_modal_dtkid" href="#">Masuk</a></li>
					<li><a alt="../../assets/detikid/register.php?transsnow" href="#" class="regis_btn box_modal_dtkid">Registrasi</a></li>    
				<?php } ?>
			</ul>

		</div>
	</nav>
	<div id="menu_" class="menu_show menu_close">
		<span></span>
		<span></span>
		<span></span>
	</div>
</header>