<section class="home-fasilitas home-others">
	<div class="container">
		<h3>Fasilitas</h3>
		<ul class="list-fasilitas">
			<li><img src="images/icon-ticket.png" alt="Tiket">Online Ticketing</li>
			<li><img src="images/icon-locker.png" alt="Locker">Locker Room</li>
			<li><img src="images/icon-shoes.png" alt="">Socks &amp; Snow Shoes</li>
			<li><img src="images/icon-nursery.png" alt="">Nursery Room</li>
			<li><img src="images/icon-toilet.png" alt="">Toilet</li>
			<li><img src="images/icon_merchandise.png" alt="">Merchandise Store</li>
		</ul>
	</div>
</section>