<div class="clearfix"></div>
<footer>
	<div class="container">
		<div class="sosmed">
			<span>Connect With Us</span>
			<a href="#"><img src="images/sm_fb.png" alt=""></a>
			<a href="#"><img src="images/sm_ig.png" alt=""></a>
			<a href="#"><img src="images/sm_tw.png" alt=""></a>
			<a href="#"><img src="images/sm_yt.png" alt=""></a>
			<a href="#"><img src="images/sm_in.png" alt=""></a>
		</div>
		<div class="nav_bottom">
			<div class="logo_bot">
				<img src="images/logo-tsw.png"
					srcset="
					images/logo-tsw.png 1x,
					images/logo-tsw@2x.png 2x,
					"  alt="Logo Trans Snow World"
					class="logo">
				<div class="powered">Powered by detiknetwork</div>
			</div>
			<div class="menu_bottom">
				<ul class="list_menu">
					<li><a href="#">Home</a></li>
					<li><a href="#">Tiket</a></li>
					<li><a href="#">Fasilitas</a></li>
					<li><a href="#">Berita</a></li>
					<li><a href="#">Bantuan</a></li>
					<li><a href="#">Kontak Kami</a></li>
				</ul>
				<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum cupidatat non proident, officia deserunt mollit anim id est laborum</p>
				<div class="copy">&copy;2019 - Trans Snow World. All Rights Reserved</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</footer>

<script type="text/javascript" src="js/plugin.js"></script>
<script type="text/javascript" src="js/controller.js"></script>