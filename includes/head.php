<head>
<title>Trans Snow World</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/style.css">
<link rel=icon type=image/png sizes=32x32 href=images/favicon-32x32.png>
<link rel=icon type=image/png sizes=16x16 href=images/favicon-16x16.png>
<script src="js/jquery.min.js"></script>
<!-- jquery ui -->
<link rel="stylesheet" href="css/jquery-ui.min.css">
<script src="js/jquery-ui.min.js"></script>
</head>