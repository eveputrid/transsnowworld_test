<html lang="en">
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="container container-order">
	<a href="list_purchased.php" class="yellow_btn back back-to-list"><img src="images/arrow-back-white.svg" alt=""> Back</a>

	<div class="detail-purchased">
		<div class="top-area">
			<div class="order-number">
				Order Number
				<span>#1239483213210</span>
			</div>
			<div class="purchased-label">
				Payment was Successful
			</div>
		</div>

		<div class="middle-area">
			<h1>Trans Snow World</h1>
			<div class="date">
				Visit Date: 
				<span>Kamis, 07 Mei 2019 | 11.00 - 13.00 WIB</span>
			</div>
			<!-- <div class="produk">
				Product Ticket
				<span>Entry Ticket</span>
				<span>Fast Track</span>
			</div> -->
			<div class="qty">
				Number Of Ticket
				<span>5 Tiket</span>
				<!-- <span>2 Tiket</span> -->
			</div>
			<div class="price">
				Price
				<span>Rp 500.000,-</span>
				<!-- <span>Rp 200.000,-</span> -->
			</div>
			<!-- <div class="total price">
				Total Price
				<span>Rp 700.000,-</span>
			</div> -->
		</div>

		<div class="main-area">
			<h3>Entry Ticket</h3>
			<div class="qrcode">
				<!-- <span>Your Entry Ticket Code</span> -->
				<img src="images/qrcode.jpg" alt="">
				<span>E-CODE: F3242IDFK</span>
			</div>

			<!-- <div class="load-more">
				<a href="#">Download as PDF</a>
			</div> -->

			<div class="howto">
				<ul class="detail-info">
					<li>
						<span>Type of Ticket</span>
						Entry Ticket
					</li>
					<li>
						<span>Number of Ticket</span>
						1
					</li>
					<li>
						<span>Visit Date</span>
						Kamis, 07 Mei 2019
					</li>
					<li>
						<span>Total Price</span>
						Rp. 300.000,-
					</li>
				</ul>
				<h5>Petunjuk Penggunaan Entry Ticket:</h5>				
				<ol>
					<li>Tiket elektronik akan dikirimkan ke alamat email yang terdaftar saat pembelian secara online di website resmi Trans Snow World atau detik.com.</li>
					<li>Tiket dapat disimpan kedalam perangkat elektronik genggam apapun, baik smartphone atau tablet.</li>
					<li>Tiket tidak perlu untuk dicetak.</li>
					<li>Pengunjung mempersiapkan QR Code yang terdapat pada tiket elektronik pada layar smartphone / tablet.</li>
					<li>Dekatkan layar smartphone / tablet yang menunjukkan QR Code kepada alat pemindai (scanner) yang tedapat di pintu masuk Trans Snow World.</li>
					<li>Jika tiket valid, pintu akan otomatis terbuka selama 15 detik.</li>
					<li>Jika dalam 15 detik pengunjung belum melewati pintu otomatis, maka tiket masih dapat digunakan kembali untuk membuka pintu.</li>
					<li>Jika pintu otomatis sudah terbuka dan pengunjung telah memasuki Trans Snow World, maka tiket tidak dapat digunakan kembali untuk membuka pintu.</li>
				</ol>
			</div>
		</div>


		<div class="main-area fast-track">
			<h3>Fast Track Code</h3>
			<div class="qrcode">
				<!-- <span>Your Fast Track Code</span> -->
				<img src="images/qrcode.jpg" alt="">
				<span>E-CODE: F3242IDFK</span>
			</div>

			<!-- <div class="load-more">
				<a href="#">Download as PDF</a>
			</div> -->

			<div class="howto">
				<ul class="detail-info">
					<li>
						<span>Type of Ticket</span>
						Fast Track
					</li>
					<li>
						<span>Number of Ticket</span>
						1
					</li>
					<li>
						<span>Visit Date</span>
						Kamis, 07 Mei 2019
					</li>
					<li>
						<span>Total Price</span>
						Rp. 300.000,-
					</li>
				</ul>
				<h5>Petunjuk Penggunaan Fast Track:</h5>				
				<ol>
					<li>Tiket elektronik akan dikirimkan ke alamat email yang terdaftar saat pembelian secara online di website resmi Trans Snow World atau detik.com.</li>
					<li>Tiket dapat disimpan kedalam perangkat elektronik genggam apapun, baik smartphone atau tablet.</li>
					<li>Tiket tidak perlu untuk dicetak.</li>
					<li>Pengunjung mempersiapkan QR Code yang terdapat pada tiket elektronik pada layar smartphone / tablet.</li>
					<li>Dekatkan layar smartphone / tablet yang menunjukkan QR Code kepada alat pemindai (scanner) yang tedapat di pintu masuk Trans Snow World.</li>
					<li>Jika tiket valid, pintu akan otomatis terbuka selama 15 detik.</li>
					<li>Jika dalam 15 detik pengunjung belum melewati pintu otomatis, maka tiket masih dapat digunakan kembali untuk membuka pintu.</li>
					<li>Jika pintu otomatis sudah terbuka dan pengunjung telah memasuki Trans Snow World, maka tiket tidak dapat digunakan kembali untuk membuka pintu.</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<?php include "includes/footer.php";?>
</body>
</html>