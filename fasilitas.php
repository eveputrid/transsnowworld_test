<html lang="en">
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="container">	
	<div class="title_page"><h1>Fasilitas</h1></div>
		
	<h4 class="feed_title">Arena</h4>
	<ul class="fasilitas-list">
		<?php for ($i=0;$i<7;$i++) { ?>
			<li>
				<a href="detail_news.php?fasilitas">
					<div class="card_fasilitas">
						<img src="images/news1.jpg" alt="">
						<div class="text">
							<h2>Zorb Ball</h2>
							<p>Duis aute irure dolor in reprehenderit in voluptate velit esse</p>						
						</div>					
					</div>				
				</a>
			</li>
		<?php } ?>	
	</ul>


	<h4 class="feed_title">Rental</h4>
	<ul class="fasilitas-list">
		<?php for ($i=0;$i<5;$i++) { ?>
			<li>
				<a href="detail_news.php?fasilitas">
					<div class="card_fasilitas">
						<img src="images/news1.jpg" alt="">
						<div class="text">
							<h2>Zorb Ball</h2>
							<p>Duis aute irure dolor in reprehenderit in voluptate velit esse</p>	
							<div class="rent-label">
								Harga Sewa<br>
								<strong>Rp 30.000</strong>/sepuasnya								
							</div>					
						</div>					
					</div>				
				</a>
			</li>
		<?php } ?>	
	</ul>


	<h4 class="feed_title">Merchandise</h4>
	<ul class="fasilitas-list">
		<?php for ($i=0;$i<5;$i++) { ?>
			<li>
				<a href="detail_news.php?fasilitas">
					<div class="card_fasilitas">
						<img src="images/news1.jpg" alt="">
						<div class="text">
							<h2>Zorb Ball</h2>
							<p>Duis aute irure dolor in reprehenderit in voluptate velit esse</p>						
						</div>					
					</div>				
				</a>
			</li>
		<?php } ?>	
	</ul>

	<h4 class="feed_title">Restaurant</h4>
	<ul class="fasilitas-list">
		<?php for ($i=0;$i<5;$i++) { ?>
			<li>
				<a href="detail_news.php?fasilitas">
					<div class="card_fasilitas">
						<img src="images/news1.jpg" alt="">
						<div class="text">
							<h2>Zorb Ball</h2>
							<p>Duis aute irure dolor in reprehenderit in voluptate velit esse</p>						
						</div>					
					</div>				
				</a>
			</li>
		<?php } ?>	
	</ul>


	<section class="sub-fasilitas">
		<h3>Fasilitas</h3>
		<ul class="list-fasilitas">
			<li><img src="images/icon-ticket.png" alt="Tiket">Online Ticketing</li>
			<li><img src="images/icon-locker.png" alt="Locker">Locker Room</li>
			<li><img src="images/icon-shoes.png" alt="">Socks &amp; Snow Shoes</li>
			<li><img src="images/icon-nursery.png" alt="">Nursery Room</li>
			<li><img src="images/icon-toilet.png" alt="">Toilet</li>
			<li><img src="images/icon_merchandise.png" alt="">Merchandise Store</li>
		</ul>
	</section>
		
</div>






<?php include "includes/footer.php";?>
</body>
</html>