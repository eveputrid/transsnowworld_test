<html lang="en">
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="container container-order">
	<h1 class="title-order-page">Halo, Bomba <span>Daftar Pembelian</span></h1>
	
	<div class="filter-order">
		<form action="#">
			<div class="filter-date">
				<span>Filter Tanggal</span>
				<input type="date">
			</div>

			<div class="filter-payment-status">
				<span>Payment Status</span>
				<div class="select-style">
					<select name="" id="">
						<option value="">Payment was Successful</option>
						<option value="">Waiting for Payment</option>
						<option value="">Cancel</option>						
					</select>
				</div>
			</div>
		</form>
	</div>

	<ul class="list-order">
		<?php for ($i=0;$i<2;$i++) { ?>
		<li>
			<div>
				<div class="order-number">
					ID Order
					<span>#1239483213210</span>
				</div>
				<div class="produk">
					Product Ticket
					<span>Entry Ticket, Fast Track</span>
					<!-- <span>Fast Track</span> -->
				</div>
				<div class="qty">
					Number of Ticket
					<span>5 Tiket, 2 Tiket</span>
					<!-- <span>2 Tiket</span> -->
				</div>
				<div class="total-price">
					Total Price
					<span>Rp 400.000,-</span>
				</div>
				<div class="payment-status success">
					Payment was Successful
				</div>
			</div>
			<div>Trans Snow World</div>
			<div>
				<div class="arrival-date">
					Visit Date: 
					<span>Kamis, 07 Mei 2019</span>
				</div>
				<a href="detail-purchased.php" class="btn-detail-ticket"> View Ticket<img src="images/right-arrow.svg" alt=""></a>
			</div>
		</li>

		<li>
			<div>
				<div class="order-number">
					ID Order
					<span>#1239483213210</span>
				</div>
				<div class="produk">
					Product Ticket
					<span>CT Corp Employee</span>
				</div>
				<div class="qty">
					Number of Ticket
					<span>2 Tiket</span>
				</div>
				<div class="total-price">
					Total Price
					<span>Rp 400.000,-</span>
				</div>
				<div class="payment-status waiting">
					Waiting for Payment
				</div>
			</div>
			<div>Trans Snow World</div>
			<div>
				<div class="arrival-date">
					Date: 
					<span>Kamis, 07 Mei 2019</span>
				</div>				
			</div>
		</li>

		<li>
			<div>
				<div class="order-number">
					ID Order
					<span>#1239483213210</span>
				</div>
				<div class="produk">
					Product Ticket
					<span>CC Mega 25%</span>
				</div>
				<div class="qty">
					Number of Ticket
					<span>2 Tiket</span>
				</div>
				<div class="total-price">
					Total Price
					<span>Rp 400.000,-</span>
				</div>
				<div class="payment-status cancel">
					Cancel
				</div>
			</div>
			<div>Trans Snow World</div>
			<div>
				<div class="arrival-date">
					Date: 
					<span>Kamis, 07 Mei 2019</span>
				</div>				
			</div>
		</li>
		<li>
			<div>
				<div class="order-number">
					ID Order
					<span>#1239483213210</span>
				</div>
				<div class="produk">
					Product Ticket
					<span>CC Mega 50%</span>
				</div>
				<div class="qty">
					Number of Ticket
					<span>2 Tiket</span>
				</div>
				<div class="total-price">
					Total Price
					<span>Rp 400.000,-</span>
				</div>
				<div class="payment-status cancel">
					Cancel
				</div>
			</div>
			<div>Trans Snow World</div>
			<div>
				<div class="arrival-date">
					Date: 
					<span>Kamis, 07 Mei 2019</span>
				</div>				
			</div>
		</li>
		<?php } ?>	
	</ul>

	<div class="load-more">
		<a href="#">Load More</a>
	</div>
	
</div>
<?php include "includes/footer.php";?>
</body>
</html>