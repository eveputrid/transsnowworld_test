<html lang="en">
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="notif">
	<picture>
		<img src="images/notif.jpg" alt="">
		<div class="msg"><h1>Terima Kasih</h1></div>
	</picture>
	<div class="text">
		<p>Pesanan anda telah berhasil dengan nomor 123456 sebesar Rp 550.000,00.
			<br>Tiket anda dipesan untuk tanggal 2019-04-17 pada Pukul 11-13.</p>
			<p>Silahkan selesaikan pembayaran anda sebelum 16 April, 2019 Pukul 14:44:33 (3 Jam).</p>
		<p>Setelah pembayaran selesai dilakukan kami akan melakukan verifikasi secara otomatis.
			<br>Tiket elektronik Anda akan dikirimkan melalui alamat Email yang Anda daftarkan.</p>

<a href="#" class="btn-notif">Ke Daftar Pembelian</a>
	</div>
</div>
<?php include "includes/footer.php";?>
</body>
</html>