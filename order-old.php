<html lang="en">
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>

<div class="container container-form">	
	<form action="notif.php">
	<div class="order-step">
		<div class="selected"><span>#1</span> PESAN TIKET</div>
		<div class=""><span>#2</span> BAYAR</div>
		<div class=""><span>#3</span> TIKET</div>
	</div>

	<div class="data-tiket">
		<div class="title-box">
			Data Tiket
		</div>
		<div class="datepick">
			<span>Tanggal</span>
					<input id="date-tiket" type="text" required placeholder="DD-MM-YYYY">
		</div>	
		<div class="timepick">
			<span>Jam Bermain</span>
			
				<select name="" id="">
					<option value="">10.00 - 12.00</option>
					<option value="">12.00 - 14.00</option>
					<option value="">14.00 - 16.00</option>
					<option value="">16.00 - 18.00</option>
				</select>
			
		</div>

		<div class="qty">
			<span>Jumlah Tiket</span>
			<select name="" id="">
				<option value="">1</option>
				<option value="">2</option>
				<option value="">3</option>
				<option value="">4</option>
			</select>
		</div>

		<div class="type">
			<span>Type of Ticket</span>
			<select name="" id="">
				<option value="">Reguler</option>
				<option value="">Promo CC Bank Mega</option>				
			</select>
		</div>

		<div class="price">
			<span>Total</span>
			<div class="sum">
				Rp 400.000				
			</div>
		</div>
	</div>

	<div class="data-user">
		<div class="title-box">
			Data Pemesan
		</div>
		<div class="form-content">
			<div class="fullname">
				<span>Nama Lengkap</span>
				<input type="text" required>
			</div>

			<div class="email">
				<span>Email</span>
				<input type="email" required>
			</div>

			<div class="phone">
				<span>Nomor Handphone</span>
				<input type="number" required>
				<a href="#">Verifikasi</a>
			</div>

			<div class="edit-phone">
				<span>Nomor Handphone</span>
				<span class="view-phone">08918723892</span>
				<a href="#">Ubah</a>
			</div>

			<div class="otp-code">
				<span class="title">Kode OTP</span>
				<input type="text" maxlength="4" required>
				<span class="countdown">02:44</span>
				<span class="info">*Masukkan kode yang telah kami kirimkan melalui SMS</span>
			</div>

			<div class="ic-type">
				<span>Tipe Identitas</span>
				<select name="" id="">
					<option value="" selected>KTP</option>
					<option value="">SIM</option>					
				</select>
			</div>

			<div class="ic-number">
				<span>Nomor Identitas</span>
				<input type="text" value="1231231241234" disabled="">
			</div>

			<div class="terms">
				<input id="check-terms" type="checkbox" required>
				<label for="check-terms"></label>
				<a href="#terms" rel="modal:open">Saya menyetujui syarat dan ketentuan yang berlaku</a>
			</div>
			<div class="submit">
				<input type="submit" value="Bayar">
			</div>
		</div>
	</div>
	</form>	
</div>

<div id="terms" class="modal">
	<div class="modal-content">
		<h4>Syarat dan Ketentuan</h4>
		  <p><strong>Title title title</strong><br>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<p><strong>Title title title</strong><br>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<p><strong>Title title title</strong><br>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<p><strong>Title title title</strong><br>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		  <div class="text-center">
		  	<a href="#" rel="modal:close" class="close_btn">Setuju</a>
		  </div>
	</div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/add_on.php";?>
</body>
</html>