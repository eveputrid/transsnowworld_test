<html lang="en">
<?php include "includes/head.php";?>
<body class="modal-tsw">

	<div id="choose-tsw" class="modal">
		<h4>Pilih di bawah ini untuk melanjutkan</h4>
		<div class="list-tsw">
			<a href="#" rel="modal:close">
				<img src="images/logo-tswjuanda@2x.png" alt="">
				<span>Trans Snow World<br>Bekasi</span>
			</a>
			<a href="#" rel="modal:close">
				<img src="images/logo-tswbintaro@2x.png" alt="">
				<span>Trans Snow World<br>Bintaro</span>
			</a>
		</div>
<script type="text/javascript" src="js/plugin.js"></script>
<script>
	$("#choose-tsw").modal({
	  escapeClose: false,
	  clickClose: false,
	  showClose: false
	});
</script>
</body>
</html>