<html lang="en">
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="container">	
	<div class="title_page">Berita & Promo</div>

	<h4 class="feed_title">Trans Snow World</h4>
	<ul class="whats-on-list alt-list no-btn">
		<li>
		<a href="#">
			<img src="images/news1.jpg" alt="">
			<div class="info">
				<h2>Main Salju di Bekasi, Kapan Saja!</h2>
				<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</p>
				
			</div>
			</a>
		</li>
		<li>
			<a href="#">
			<div class="image-container">
				<img src="images/news2.jpg" alt="">
			</div>
			<div class="info">
				<h2>Main Salju di Bekasi, Kapan Saja! Main Salju di Bekasi, Kapan Saja! Main</h2>					
			</div>
			</a>
		</li>
		<li>
			<a href="#">
			<div class="image-container">
				<img src="images/news3.jpg" alt="">
			</div>
			<div class="info">
				<h2>Main Salju di Bekasi, Kapan Saja!</h2>					
			</div>
			</a>
		</li>

		<?php for ($i=0;$i<8;$i++) { ?>
			<li>
				<a href="#" class="card_news">
					<div class="ratio16_9 box_img">
						<div class="img_con lqd">
							<img src="images/news1.jpg" alt="">
						</div>
					</div>
					<div class="text">
						<h2>Main Salju Makin Seru dengan Promo Paket Couple Milo & Internet</h2>
					</div>
			</a>
			</li>
		<?php } ?>
	</ul>	

	<h4 class="feed_title">Detik Network</h4>

	<ul class="news-list">
	<?php for ($i=0;$i<16;$i++) { ?>
			<li>
				<a href="#" class="card_news no-btn">
				<div class="ratio16_9 box_img">
					<div class="img_con lqd">
						<img src="images/news1.jpg" alt="">
					</div>
				</div>
				<div class="text">
				<h2>Main Salju Makin Seru dengan Promo Paket Couple Milo & Internet</h2>
				</div>
				<div class="label">!nsertlive</div>
			</a>
		</li>
		<?php } ?>
	</ul>

<div class="pagination text-center mgt-16 mgb-16">
	<a href="" class="pagination__item">Prev</a>
	<a href="" class="pagination__item pagination__item--active">1</a>
	<a href="" class="pagination__item">2</a>
	<a href="" class="pagination__item">3</a>
	<a href="" class="pagination__range">...</a>
	<a href="" class="pagination__item">16</a>
	<a href="" class="pagination__item">Next</a>
</div>

	<!-- <h4 class="feed_title">Detik Network</h4>
	<ul class="news-list">
		<?php for ($i=0;$i<4;$i++) { ?>
			<li>
				<div class="card_news">
					<div class="ratio16_9 box_img">
						<div class="img_con lqd">
							<img src="images/news1.jpg" alt="">
						</div>
					</div>
					<div class="text">
						<h2>Main Salju di Bekasi, Kapan Saja!</h2>
						<a href="#" class="btn_more">View Detail</a>
					</div>
					<div class="label">detikcom</div>
				</div>				
			</li>
		<?php } ?>	

		<?php for ($i=0;$i<4;$i++) { ?>
			<li>
				<div class="card_news">
					<div class="ratio16_9 box_img">
						<div class="img_con lqd">
							<img src="images/news1.jpg" alt="">
						</div>
					</div>
					<div class="text">
						<h2>Main Salju di Bekasi, Kapan Saja!</h2>
						<a href="#" class="btn_more">View Detail</a>
					</div>
					<div class="label">CNN Indonesia</div>
				</div>				
			</li>
		<?php } ?>	

		<?php for ($i=0;$i<4;$i++) { ?>
			<li>
				<div class="card_news">
					<div class="ratio16_9 box_img">
						<div class="img_con lqd">
							<img src="images/news1.jpg" alt="">
						</div>
					</div>
					<div class="text">
						<h2>Main Salju di Bekasi, Kapan Saja!</h2>
						<a href="#" class="btn_more">View Detail</a>
					</div>
					<div class="label">CNBC</div>
				</div>				
			</li>
		<?php } ?>	

		<?php for ($i=0;$i<4;$i++) { ?>
			<li>
				<div class="card_news">
					<div class="ratio16_9 box_img">
						<div class="img_con lqd">
							<img src="images/news1.jpg" alt="">
						</div>
					</div>
					<div class="text">
						<h2>Main Salju di Bekasi, Kapan Saja!</h2>
						<a href="#" class="btn_more">View Detail</a>
					</div>
					<div class="label">HaiBunda</div>
				</div>				
			</li>
		<?php } ?>	

		<?php for ($i=0;$i<4;$i++) { ?>
			<li>
				<div class="card_news">
					<div class="ratio16_9 box_img">
						<div class="img_con lqd">
							<img src="images/news1.jpg" alt="">
						</div>
					</div>
					<div class="text">
						<h2>Main Salju di Bekasi, Kapan Saja!</h2>
						<a href="#" class="btn_more">View Detail</a>
					</div>
					<div class="label">20detik</div>
				</div>				
			</li>
		<?php } ?>	

		<?php for ($i=0;$i<4;$i++) { ?>
			<li>
				<div class="card_news">
					<div class="ratio16_9 box_img">
						<div class="img_con lqd">
							<img src="images/news1.jpg" alt="">
						</div>
					</div>
					<div class="text">
						<h2>Main Salju di Bekasi, Kapan Saja!</h2>
						<a href="#" class="btn_more">View Detail</a>
					</div>
					<div class="label">!nsertlive</div>
				</div>				
			</li>
		<?php } ?>		
	</ul> -->
		
</div>

<?php include "includes/footer.php";?>
</body>
</html>