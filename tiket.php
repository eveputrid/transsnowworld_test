<html lang="en">
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="container">
	<h1 class="title_page">Tiket</h1>


	<section class="home-tiket">
	<form action="tiket.php">
	<div class="order-tiket order-tiket-index">		
		<div class="title">BOOKING <span>TIKET</span></div>

		<div class="detail-tiket">
			<!-- <div class="timepick location">
				<span>Location</span>
				<div class="select-style">
					<select name="" id="">						
						<option value="" data-content="TSW Juanda">Trans Snow World Juanda</option>
						<option value="" data-content="TSW Bintaro">Trans Snow World Bintaro</option>
						<option value="" data-content="TSW Juanda 2">Trans Snow World Juanda 2</option>
						<option value="" data-content="TSW Bintaro 2">Trans Snow World Bintaro 3</option>
					</select>
				</div>
			</div> -->

			<div class="datepick">
				<span>Visit Date</span>
				<input id="date-tiket" type="text" placeholder="DD-MM-YYYY" required>
			</div>

			<!-- <div class="timepick">
				<span>Time</span>
				<div class="select-style">
					<select name="" id="">						
						<option value="">10.00 - 12.00 WIB</option>
						<option value="">12.00 - 14.00 WIB</option>
						<option value="">14.00 - 16.00 WIB</option>
						<option value="">16.00 - 18.00 WIB</option>
					</select>
				</div>
			</div> -->

				<div class="timepick">
				<span>Total Ticket</span>
					<div class="select-style">
						<select name="" id="">
							<option value="">1</option>
							<option value="">2</option>
							<option value="">3</option>
							<option value="">4</option>
							option value="">5</option>
						</select>
					</div>
				</div>

			<!-- <div class="price">
				<span>Harga</span>
				<div class="sum">
					Rp 200.000
					<sub>/Tiket</sub>
				</div>
			</div> -->
		</div>

		<!-- <a href="#" class="booking_btn">
			Booking Now <img src="images/right-arrow.svg" alt="">
		</a> -->
		<input type="submit" class="booking_btn" value="Book Now">		
	</div>
	</form>
</section>
	
	<div class="detail_page detail_tiket">
		<div class="title">Syarat Ketentuan</div>
		<ol>
			<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit dolores adipisci, impedit ipsum voluptatibus dolore. Cupiditate quae distinctio, veritatis eligendi, iusto laborum similique fugit et ea corrupti cumque, sequi ad!</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate, perferendis, officia laboriosam eaque eligendi et aut iusto cumque est, fugiat neque totam. Laboriosam ipsam accusamus eaque sed, ipsa autem aliquam!</li>
		</ol>

	</div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/add_on.php";?>
</body>
</html>