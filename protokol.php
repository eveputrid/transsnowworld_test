<html lang="en">
    <?php include "includes/head.php";?>
    <body>

        <?php include "includes/header.php";?>
        <!--<div class="container wrapper16-9"> <section class="video-area center-text">
        <div class="video_detail"> <div class="video_detail__wrapper"> <iframe
        src="https://20.detik.com/embed/191220064?smartautoplay=false" frameborder="0"
        width="960" height="540" scrolling="no" allowfullscreen="true" ></iframe>
        <iframe class="youtube-player" id="yt-player-XFDX2iy5njw" frameborder="0"
        allowfullscreen="1" allow="accelerometer; autoplay; encrypted-media; gyroscope;
        picture-in-picture" title="YouTube video player" width="960" height="540"
        src="https://20.detik.com/embed/191220064?smartautoplay=false"></iframe> </div>
        </div> </section> </div>-->
        <section class="video-area center-text">
            <div class="video_detail">
                <div class="video_detail__wrapper">
                    <iframe
                        width="560"
                        height="315"
                        src="https://www.youtube.com/embed/Iw7R7CyycVQ"
                        frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen="allowfullscreen"></iframe>
                </div>
            </div>
        </section>

        <section class="sub-fasilitas fasilitas-protokol">
            <h2>Cara Aman Bermain Nyaman</h2>
            <span class="text-protokol-first">
                Di era Adaptasi Kebiasaan Baru tentunya anda semua telah rindu akan berwisata
                dan menghabiskan momen bersama orang tersayang. Trans Snow World <?php include "includes/query.php"; ?> sebagai salah
                satu wisata salju terbesar yang ada di Indonesia pun telah siap untuk kembali
                menyambut Anda dan keluarga. Tetapi tentunya dengan beberapa protokol kesehatan
                yang harus dipenuhi oleh para karyawan maupun pengunjung.
            </span>

            <h2>Yuk lihat protokol kesehatan kami!</h2>
            <ul class="list-fasilitas">

                <li>
                    <img src="images/protokol-0.png" alt="protokol-1">
                    <div class="desc-protokol">
                        <h3>Aplikasi Peduli Lindungi</h3>
                        <span>Pengunjung wajib memindai QR code dengan aplikasi Peduli Lindungi di pintu masuk Transpark Mall <?php include "includes/query.php"; ?>
  untuk masuk ke Trans Snow World <?php include "includes/query.php"; ?>.</span>
                    </div>
                </li>
                <li>
                    <img src="images/protokol-1.png" alt="protokol-1">
                    <div class="desc-protokol">
                        <h3>Wajib Masker</h3>
                        <span>Setiap pengunjung diwajibkan memakai masker selama berada di dalam area
                            Trans Snow World <?php include "includes/query.php"; ?> tanpa terkecuali.</span>
                    </div>
                </li>
                <li>
                    <img src="images/protokol-2.png" alt="protokol-1">
                    <div class="desc-protokol">
                        <h3>Pengukuran Suhu Tubuh</h3>
                        <span>Pengukuran suhu tubuh akan dilakukan sebelum memasuki area Trans Snow
                            World <?php include "includes/query.php"; ?>. Bagi Anda dengan suhu tubuh normal 36,1 - 37,2 Celcius akan diperbolehkan
                            masuk</span>
                    </div>
                </li>
                <li>
                    <img src="images/protokol-3.png" alt="protokol-1">
                    <div class="desc-protokol">
                        <h3>Jaga Jarak</h3>
                        <span>Tetap memperhatikan jaga jarak minimal 1,5 meter, baik pada saat
                            berjalan-jalan di area maupun pada saat di antrian.</span>
                    </div>
                </li>
                <li>
                    <img src="images/protokol-4.png" alt="protokol-1">
                    <div class="desc-protokol">
                        <h3>Disinfektan Area</h3>
                        <span>Secara rutin petugas Trans Snow World <?php include "includes/query.php"; ?> akan membersihkan area Trans Snow
                            World dan menyemprot setiap area dengan cairan disinfektan.</span>
                    </div>
                </li>
                <li>
                    <img src="images/protokol-5.png" alt="protokol-1">
                    <div class="desc-protokol">
                        <h3>Disinfektan Alat Sewa</h3>
                        <span>Secara rutin petugas Trans Snow World <?php include "includes/query.php"; ?> akan menyemprot alat sewa seperti
                            Sepatu, Sledge, Ski dan alat bermain lainnya dengan cairan disinfektan</span>
                    </div>
                </li>

                <li>
                    <img src="images/protokol-6.png" alt="protokol-1">
                    <div class="desc-protokol">
                        <h3>Cuci Tangan</h3>
                        <span>Disediakan tempat mencuci tangan bagi customer di dalam area Trans Snow World <?php include "includes/query.php"; ?></span>
                    </div>
                    
                </li>

                <li>
                    <img src="images/protokol-7.png" alt="protokol-1">
                    <div class="desc-protokol">
                        <h3>Sanksi Pelanggaran</h3>
                        <span>Polisi Masker yang bertugas akan menjaga dan mengingatkan selalu untuk
                            menggunakan masker dan menjaga jarak antar pengunjung. Bagi pengunjung yang
                            tidak tertib akan mendapatkan teguran</span>
                    </div>
                </li>

                <li>
                    <img src="images/protokol-8.png" alt="protokol-1">
                    <div class="desc-protokol">
                        <h3>Seluruh karyawan Trans Snow World <?php include "includes/query.php"; ?> sudah divaksinasi</h3>
                        <span>Demi kenyamanan dan keamanan kita bersama, seluruh karyawan Trans Snow World <?php include "includes/query.php"; ?> yang bertugas sudah divaksinasi dan dalam keadaan sehat saat bertugas. Petugas kami juga menggunakan masker, face shield dan sarung tangan saat bertugas.</span>
                    </div>
                </li>

                <li>
                    <img src="images/protokol-9.png" alt="protokol-1">
                    <div class="desc-protokol">
                        <h3>Tanpa Kontak Fisik</h3>
                        <span>Dengan melakukan pembelian tiket secara online, customer bisa langsung
                            memasuki wilayah Trans Snow World <?php include "includes/query.php"; ?> tanpa harus menukarkan tiket terlebih dahulu
                            (hanya tinggal memperlihatkan QR Code)</span>
                    </div>
                </li>
                <li>
                    <img src="images/protokol-10.png" alt="protokol-1">
                    <div class="desc-protokol">
                        <h3>Tersedia Handsanitizer</h3>
                        <span>Disediakan handsanitizer di area Trans Snow World <?php include "includes/query.php"; ?> yang diletakkan di area
                            ganti sepatu, di dekat rides, dan restaurant area.</span>
                    </div>
                </li>
            </ul>
        </section>
        <section class="faq">
            <h2>FAQ</h2>
            <ul class="accordion">
            <li>
                    <h5 class="title_acc buka">
                        Apakah wajib menggunakan Aplikasi Peduli Lindungi untuk masuk ke Trans Snow World <?php include "includes/query.php"; ?>?</h5>
                    <div class="box_acc" style="display:block;">
                        <p>Pengunjung wajib memindai QR code dengan aplikasi Peduli Lindungi di pintu
                            masuk Transpark Mall <?php include "includes/query.php"; ?> untuk masuk ke Trans Snow World <?php include "includes/query.php"; ?>.</p>
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li>
                    <h5 class="title_acc">Apakah wajib menggunakan masker?</h5>
                    <div class="box_acc">
                        <p>Diwajibkan memakai masker bagi karyawan dan pengunjung selama berada di dalam
                            area Trans Snow World <?php include "includes/query.php"; ?>.</p>
                        <div class="clearfix"></div>
                    </div>
                </li>

                <li>
                    <h5 class="title_acc">Apakah ada pengecekan suhu tubuh?</h5>
                    <div class="box_acc">
                        <p>Pengukuran suhu tubuh akan dilakukan sebelum memasuki area Trans Snow World <?php include "includes/query.php"; ?>
                            oleh petugas. Suhu tubuh normal manusia 36,1 derajat Celcius - 37,2 derajat
                            Celcius akan diperbolehkan masuk.</p>
                        <div class="clearfix"></div>
                    </div>
                </li>

                <li>
                    <h5 class="title_acc">Apakah terdapat handsanitizer di setiap area?</h5>
                    <div class="box_acc">
                        <p>Disediakan handsanitizer di area Trans Snow World <?php include "includes/query.php"; ?> yang diletakan di setiap
                            sudut ruangan.</p>
                        <div class="clearfix"></div>
                    </div>
                </li>

                <li>
                    <h5 class="title_acc">Apakah diharuskan menjaga jarak?</h5>
                    <div class="box_acc">
                        <p>Akan ada petugas yang berkeliling untuk menegur pengunjung yang tidak menjaga
                            jarak.</p>
                        <div class="clearfix"></div>
                    </div>
                </li>

                <li>
                    <h5 class="title_acc">Apakah ada penyemprotan disinfektan pada area bermain?</h5>
                    <div class="box_acc">
                        <p>Secara rutin petugas Trans Snow World <?php include "includes/query.php"; ?> akan membersihkan area Trans Snow World <?php include "includes/query.php"; ?>
                            dan menyemprot setiap area bermain dan peralatan seperti sepatu, Sledge, Ski dan
                            lain sebagainya yang akan disewakan dengan cairan disinfektan.</p>
                        <div class="clearfix"></div>
                    </div>
                </li>

                <li>
                    <h5 class="title_acc">Bagaimana dengan tempat cuci tangan, adakah tempat cuci tangan di setiap area?</h5>
                    <div class="box_acc">
                        <p>Disediakan tempat mencuci tangan bagi customer di dalam area Trans Snow World <?php include "includes/query.php"; ?>.</p>
                        <div class="clearfix"></div>
                    </div>
                </li>

                <li>
                    <h5 class="title_acc">Apakah tim Trans Snow World <?php include "includes/query.php"; ?> dapat memastikan pengunjung menerapkan peraturan?</h5>
                    <div class="box_acc">
                        <p>Petugas dari Trans Snow World <?php include "includes/query.php"; ?> akan memberikan teguran dan sangsi bagi
                            siapapun yang melanggar protokol kesehatan yang ada.</p>
                        <div class="clearfix"></div>
                    </div>
                </li>

                <li>
                    <h5 class="title_acc">Apakah seluruh karyawan Trans Snow World <?php include "includes/query.php"; ?> sudah divaksinasi?</h5>
                    <div class="box_acc">
                        <p>Seluruh karyawan Trans Snow World <?php include "includes/query.php"; ?> yang bertugas sudah divaksinasi dan dalam keadaan sehat saat bertugas..</p>
                        <div class="clearfix"></div>
                    </div>
                </li>

                <li>
                    <h5 class="title_acc">Bagaimana cara memesan tiket Trans Snow World <?php include "includes/query.php"; ?>?</h5>
                    <div class="box_acc">
                        <p>Pemesanan tiket dapat dilakukan secara online melalui website Trans Snow
                            World <?php include "includes/query.php"; ?>, kamu hanya perlu menginput tanggal bermain dan memilih jumlah tiket lalu
                            klik “Book Now” setelahnya kamu akan mendapatkan informasi mengenai tiket Trans
                            Snow World yang akan dikirimkan melalui email. Dengan membeli secara online kamu
                            juga terhindar dari kontak fisik tanpa harus menukarkan tiket terlebih dahulu
                            (hanya tinggal memperlihatkan QR Code).</p>
                        <div class="clearfix"></div>
                    </div>
                </li>

                <li>
                    <h5 class="title_acc">Apakah pembelian tiket On The Spot tetap dibuka?</h5>
                    <div class="box_acc">
                        <p>Ya, pemesanan tiket secara offline (On The Spot) juga dibuka, tentunya dengan
                            standar protokol kesehatan yang ada. Namun kami menyarankan agar pengunjung
                            membeli secara online agar bisa langsung memasuki wilayah Trans Snow World <?php include "includes/query.php"; ?> tanpa
                            harus menukarkan tiket terlebih dahulu (hanya tinggal memperlihatkan QR Code).</p>
                        <div class="clearfix"></div>
                    </div>
                </li>

                <li>
                    <h5 class="title_acc">Berapa lama masa periode tiket Trans Snow World <?php include "includes/query.php"; ?>?</h5>
                    <div class="box_acc">
                        <p>Untuk masa berlaku tiket dengan pembelian secara online, tiket akan berlaku
                            pada tanggal kedatanganmu, kamu dapat langsung menghubungi hotline Trans Snow
                            World <?php include "includes/query.php"; ?> untuk informasi lebih lanjut.</p>
                        <div class="clearfix"></div>
                    </div>
                </li>

            </ul>
        </section>

    </div>

    <div class="text-center bgwhite protokol-book">
        <a href="#" class="book-now">Pesan Tiket Sekarang</a>
    </div>

    <?php include "includes/footer.php";?>
    <?php include "includes/add_on.php";?>
    <script>

        // S:ACCORDION
        $(document).ready(function () {
            $(".accordion .title_acc").click(function () {
                if ($(this).next("div").is(":visible")) {
                    $(".accordion .title_acc").removeClass("buka");
                    $(this)
                        .next("div")
                        .slideUp("fast");
                } else {
                    $(".accordion .box_acc").slideUp("fast");
                    $(".accordion .title_acc").removeClass("buka");
                    $(this).addClass("buka");
                    $(this)
                        .next("div")
                        .slideToggle("fast");
                }
            });
        });
        // E:ACCORDION
    </script>
</body>
</html>