 

function liquid($param1) {
  var lqd = $($param1);
  $(lqd).each(function(index){
    var img_src = $(this).find('img').attr( "src" );
    if($param1 == ".lqd"){
      $(this).find('img').hide();
      $(this).css('background-image', 'url(' + img_src + ')');
    }
    else {
      $(this).append('<div class="lqd_img" style="background-image:url('+ img_src +');"></div>');
    }
  });
  return;
}

$(document).ready(function() {
    liquid('.lqd');
    liquid('.lqd_block');

    $('.datepick input').datepicker();
});

function init() {
    window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 0,
            header = document.querySelector("#header");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
            // $("body").css("padding-top","104px");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
                // $("body").css("padding-top","0px");
            }
        }
    });
}
window.onload = init();

if ($(window).width() < 850) {
      //menu mobile
      $("#menu_").click(function (){
        if($(this).hasClass('menu_close')){
          $('nav').addClass('menu_show');
          $('#menu_').addClass('menu_close_style');
          $('#menu_').removeClass('menu_close');
          $('#menu_').removeClass('menu_close');
          $('body').css('overflow','hidden');
          //$('body').css('position','fixed');
        }
        else{
          $('nav').removeClass('menu_show');
          $('#menu_').removeClass('menu_close_style');
          $('#menu_').addClass('menu_close');
          $('body').css('overflow','scroll');
          //$('body').css('position','unset');

        }
      });
};
// MODALBOX
    $(".box_modal_dtkid").click(function() {
        //alert();
        if ($('div').attr('id') == 'pop_box_now') {} else {
            $(this).removeAttr('href');
            var src = $(this).attr("alt");
            size = src.split('|');
            url = size[0],
                width = '100%',
                height = '100%'

            $("body").append("<div class='pop_box_dtkid scrollable' id='pop_box_now'><iframe frameborder='0' id='framebox' src=''></iframe></div>");
            $("#framebox").animate({
                height: height,
                width: width,
            }, 0).attr('src', url).css('position', 'absolute').css('top', '0').css('left', '0').css('z-index','10');
            $("body").css('overflow', 'hidden');
        }
    });

    function pop_next(src) {
        size = src.split('|');
        url = size[0],
            width = '100%',
            height = '100%'
        $("#framebox").animate({
            height: height,
            width: width,
        }, 0).attr('src', url).css('position', 'absolute').css('top', '0').css('left', '0');
    };

    function closepop() {
        $("body").css('overflow', 'scroll');
        $("#pop_box_now").remove();
    };

    $(".close_box").click(function (){
        closepop();
    });
    $(".close_box_in").click(function (){
        closepop();
    });

    
    // close notif 
    $('.header__notif .close-notif').click(function(event) {
      headerHeight = $('header').height() - $('.header__notif').height() - 27;
      $('.header__notif').addClass('close');
      event.preventDefault();
    });



