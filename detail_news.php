<html lang="en">
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="container">
	<?php if (isset($_GET['fasilitas'])) { ?>
		<div class="breadcrumb">
			<a href="#">Home</a> / 
			<a href="#">Fasilitas</a> / 
			<a href="#">Title fasilitas</a>
		</div>
	<?php } else { ?>
		<div class="breadcrumb">
			<a href="#">Home</a> / 
			<a href="#">Berita</a> / 
			<a href="#">Judul</a>
		</div>
	<?php } ?>

	<div class="detail_berita detail_page">
		<?php if (isset($_GET['fasilitas'])) { ?>
			<div class="title-fasilitas">
				<h1>Zorb Ball</h1>
				<div class="rent-label">
					Harga Sewa<br>
					<strong>Rp 30.000</strong><sub>/sepuasnya</sub>
				</div>
				<div class="share_box">
					<span>Share</span>
					<a href="#"><img src="images/sm_fb.png" alt=""></a>
					<a href="#"><img src="images/sm_tw.png" alt=""></a>
					<a href="#"><img src="images/sm_in.png" alt=""></a>
					<a href="#" class="msg-app"><img src="images/sm_wa.png" alt=""></a>
					<a href="#" class="msg-app"><img src="images/sm_line.png" alt=""></a>
				</div>
			</div>
		<?php } else { ?>
		<div class="title">
			<h1>Main Salju di Bekasi, Kapan Saja!</h1>
			<div class="info">Minggu, 17 Maret 2019 - Oleh Bomba</div>

			<div class="share_box">
				<span>Share</span>
				<a href="#"><img src="images/sm_fb.png" alt=""></a>
				<a href="#"><img src="images/sm_tw.png" alt=""></a>
				<a href="#"><img src="images/sm_in.png" alt=""></a>
				<a href="#" class="msg-app"><img src="images/sm_wa.png" alt=""></a>
				<a href="#" class="msg-app"><img src="images/sm_line.png" alt=""></a>
			</div>
		</div>
		<?php } ?>
		<div class="img_detail">
			<img src="images/news1.jpg" alt="">
		</div>
		<div class="detail_text">
			Bagi Anda yang ingin merasakan bermain salju, kini tidak perlu jauh-jauh bepergian keluar negeri, karena akan segera hadir wahana bermain salju di dalam ruangan dengan luas yang mencapai 2.000 meter persegi, yaitu Trans Snow World Juanda, Bekasi. Meskipun Bekasi dikenal sebagai daerah yang memilki suhu lebih panas dibandingkan daerah lain, persepsi itu nampaknya akan segera hilang karena Trans Snow World Juanda memungkinkan Anda dan keluarga untuk menikmati butiran-butiran lembut salju sepuasnya seperti di negara empat musim.
			<br><br>
			Tidak hanya membuat bola dan boneka salju atau naik kereta gantung, di Trans Snow World Juanda, Anda juga bisa berseluncur di atas salju dengan snow board. Bahkan dengan areanya yang begitu luas, Anda dapat bermain ski dengan sensasi seperti di pegunungan Hokkaido, Jepang.
			<img src="images/news2.jpg" alt="">
			<br><br>
			Trans Snow World Juanda menggunakan salju asli yang dibuat dari mesin khusus. Namun, meskipun salju yang ada itu asli, suhu ruangan di Trans Snow World Juanda hanya berkisar di angka 10 sampai dengan 15 derajat Celsius, sehingga Anda tidak akan kedinginan. Selain sebagai hiburan yang atraktif, Trans Snow World dapat menjadi sarana edukasi bagi anak-anak untuk mengenal iklim berbeda di negara empat musim, di saat para orang dewasa dapat mencicipi rasanya mendaki bukit salju, menari di bawah hujan salju, hingga bermain bola salju yang seru dan menyenangkan.
			<br><br>
			Trans Snow World Juanda, berada di area Trans Park Juanda, Bekasi yang hanya berjarak 100 meter dari stasiun Bekasi Timur. Jika Anda berada di kawasan Sudirman-Thamrin Jakarta, cukup menggunakan commuter line dengan biaya hanya Rp 3.500 untuk tiba di Trans Snow World Juanda. Sedangkan bila Anda menggunakan kendaraan sendiri, Anda dapat mengakses tol Bekasi Timur maupun tol Bekasi Barat. Begitupun dengan moda tranportasi lainnya, seperti Transjakarta dan LRT yang sangat mudah di akses untuk menuju lokasi tersebut.
		</div>
		<div class="share_box" align="center">
			<span>Share</span>
			<a href="#"><img src="images/sm_fb.png" alt=""></a>
			<a href="#"><img src="images/sm_tw.png" alt=""></a>
			<a href="#"><img src="images/sm_in.png" alt=""></a>
			<a href="#" class="msg-app"><img src="images/sm_wa.png" alt=""></a>
			<a href="#" class="msg-app"><img src="images/sm_line.png" alt=""></a>
		</div>
	</div>

	<div class="title_page">Fasilitas Lainnya</div>
	<div class="list_4">
		<a href="#" class="card_news no-btn">
			<div class="ratio16_9 box_img">
				<div class="img_con lqd">
					<img src="images/news1.jpg" alt="">
				</div>
			</div>
			<div class="text">
			<h2>Main Salju Makin Seru dengan Promo Paket Couple Milo & Internet</h2>
			</div>
		</a>
		<a href="#" class="card_news no-btn">
			<div class="ratio16_9 box_img">
				<div class="img_con lqd">
					<img src="images/news1.jpg" alt="">
				</div>
			</div>
			<div class="text">
				<h2>Main Salju di Bekasi, Kapan Saja!</h2>
			</div>
		</a>
		<a href="#" class="card_news no-btn">
			<div class="ratio16_9 box_img">
				<div class="img_con lqd">
					<img src="images/news1.jpg" alt="">
				</div>
			</div>
			<div class="text">
				<h2>Main Salju di Bekasi, Kapan Saja!</h2>
			</div>
		</a>
		<a href="#" class="card_news no-btn">
			<div class="ratio16_9 box_img">
				<div class="img_con lqd">
					<img src="images/news1.jpg" alt="">
				</div>
			</div>
			<div class="text">
				<h2>Main Salju Makin Seru dengan Promo Paket Couple Milo & Internet</h2>
			</div>
		</a>
	</div>
	<div class="clearfix"></div>
</div>
<?php include "includes/footer.php";?>
</body>
</html>