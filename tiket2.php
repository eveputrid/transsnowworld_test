<html lang="en">
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="container">
	<!-- <h1 class="title_page">Tiket</h1> -->
	<!-- <section class="home-tiket">
		<div class="desktop">
			<a href="order.php"><img src="images/select_desktop.jpg" alt=""></a>
		</div>
		<div class="mobile">
			<a href="order.php"><img src="images/select_mobile.jpg" alt=""></a>
		</div>
	</section> -->

	<div class="order-step step1">
		<div class="selected"><span>#1</span> CHOOSE PRODUCT TICKET</div>
		<div class=""><span>#2</span> BOOKING TICKET</div>
		<div class=""><span>#3</span> PAYMENT</div>
		<div class=""><span>#4</span> GET YOUR TICKET</div>
	</div>

	<section class="tipe-tiket">
		
		<div class="tipe-tiket__item">
			<h2>WEEKEND ONLY ADMISSION</h2>
			<div class="price">
				<span>Rp</span>280.000
				<sub>/person</sub>
			</div>
			<div class="info">Normal Ticket</div>
			<div class="info">Price Include Tax</div>
			<div class="info">Ticket Valid From 0 Years</div>
			<div class="info">Max. Purchase 4 tickets</div>
			<a href="order.php"> Buy Now <img src="images/right-arrow.svg" alt=""></a>
		</div>

		<div class="purple tipe-tiket__item">
			<h2>CT CORP<br>EMPLOYEE</h2>
			<div class="price">
				<span>Rp</span>100.000
				<sub>/person</sub>
			</div>
			<div class="info">Only for CT Corp Employee</div>
			<div class="info">Employee must Entrance the Gate</div>
			<div class="info">Ticket Valid From 0 Years</div>			
			<a href="order.php"> Buy Now <img src="images/right-arrow.svg" alt=""></a>
		</div>

		<div class="orange promo-label tipe-tiket__item">
			<h2>Monday - Thursday<br>Admission</h2>
			<div class="price">
				<span>Rp</span>210.000
				<sub>/person</sub>
				<div class="disc">Rp 280.000</div>
			</div>
			<div class="info">CC Mega Promo</div>
			<div class="info">Price Include Tax</div>
			<div class="info">Ticket Valid From 0 Years</div>
			<div class="info">Max. Purchase 4 tickets</div>
			<a href="order.php"> Buy Now <img src="images/right-arrow.svg" alt=""></a>
		</div>

		<div class="green promo-label tipe-tiket__item">
			<h2>CC MEGA 50%</h2>
			<div class="price">
				<span>Rp</span>140.000
				<sub>/person</sub>
				<div class="disc">Rp 280.000</div>
			</div>
			<div class="info">CC Mega Promo</div>
			<div class="info">Price Include Tax</div>
			<div class="info">Ticket Valid From 0 Years</div>
			<div class="info">Max. Purchase 4 tickets</div>
			<a href="order.php"> Buy Now <img src="images/right-arrow.svg" alt=""></a>
		</div>
		<div class="green promo-label tipe-tiket__item">
			<h2>CC MEGA 50%</h2>
			<div class="price">
				<span>Rp</span>140.000
				<sub>/person</sub>
				<div class="disc">Rp 280.000</div>
			</div>
			<div class="info">CC Mega Promo</div>
			<div class="info">Price Include Tax</div>
			<div class="info">Ticket Valid From 0 Years</div>
			<div class="info">Max. Purchase 4 tickets</div>
			<a href="order.php"> Buy Now <img src="images/right-arrow.svg" alt=""></a>
		</div>
			
	</section>
	<div class="detail_page detail_tiket">
		<div class="title">Syarat Ketentuan</div>
		<ol>
			<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit dolores adipisci, impedit ipsum voluptatibus dolore. Cupiditate quae distinctio, veritatis eligendi, iusto laborum similique fugit et ea corrupti cumque, sequi ad!</li>
			<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate, perferendis, officia laboriosam eaque eligendi et aut iusto cumque est, fugiat neque totam. Laboriosam ipsam accusamus eaque sed, ipsa autem aliquam!</li>
		</ol>

	</div>
</div>
<?php include "includes/footer.php";?>
<script type="text/javascript" src="js/slick.min.js"></script>
<script>

	$('.tipe-tiket').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  dots: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    }    
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
</script>
</body>
</html>