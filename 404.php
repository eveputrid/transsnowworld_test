<html lang="en">
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="not-found">
	<div class="container">
		<h1>404<span>Page Not Found</span></h1>
		<p>The page you are looking for might have been removed<br>
had its name changed or is temporarily unavailable</p>
<a href="index.php" class="blue_btn">Go to Homepage <img src="images/right-arrow.svg" alt=""></a>
	</div>
</div>
<?php include "includes/footer.php";?>
</body>
</html>