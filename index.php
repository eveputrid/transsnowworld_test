<html lang="en">
<?php include "includes/head.php";?>
<body>


<?php include "includes/header.php";?>


<section class="home-hero">
	<ul class="list_hero">
		<li class="aspect-ratio-box">
			<picture class="aspect-ratio-box-inside">
				<img src="images/tes1.jpg" alt="">
			</picture>
			<div class="container">
				<p>The First & The Biggest<br>Snow Indoor Theme Park</p>
				<p><span>In Indonesia</span></p>
				<!-- <a href="#" class="yellow_btn link">Show More <img src="images/right-arrow.svg" alt=""></a> -->
			</div>
		</li>
		<li class="aspect-ratio-box">
			<picture class="aspect-ratio-box-inside">
				<img src="images/tes2.jpg" alt="">
			</picture>
			<div class="container">
				<p>The First & The Biggest<br>Snow Indoor Theme Park</p>
				<p><span>In Indonesia</span></p>
				<a href="#" class="yellow_btn link">Show More <img src="images/right-arrow.svg" alt=""></a>
			</div>
		</li>
		<li class="aspect-ratio-box">
			<picture class="aspect-ratio-box-inside">
				<img src="images/hero-main2.jpg" alt="">
			</picture>
			<div class="container">
				<p>The First & The Biggest<br>Snow Indoor Theme Park</p>
				<p><span>In Indonesia</span></p>
				<a href="#" class="yellow_btn link">Show More <img src="images/right-arrow.svg" alt=""></a>
			</div>
		</li>		
	</ul>
</section>

<section class="home-tiket">
	<form action="tiket.php">
	<div class="order-tiket">		
		<div class="title">BOOKING <span>TIKET</span></div>

		<div class="detail-tiket">
			<!-- <div class="timepick location">
				<span>Location</span>
				<div class="select-style">
					<select name="" id="">						
						<option value="" data-content="TSW Juanda">Trans Snow World Juanda</option>
						<option value="" data-content="TSW Bintaro">Trans Snow World Bintaro</option>
						<option value="" data-content="TSW Juanda 2">Trans Snow World Juanda 2</option>
						<option value="" data-content="TSW Bintaro 2">Trans Snow World Bintaro 3</option>
					</select>
				</div>
			</div> -->

			<div class="datepick">
				<span>Visit Date</span>
				<input id="date-tiket" type="text" required placeholder="DD-MM-YYYY">
			</div>
			
			<div class="timepick">
				<span>Total Tiket</span>
				<div class="select-style">
					<select name="" id="">
						<option value="">1</option>
						<option value="">2</option>
						<option value="">3</option>
						<option value="">4</option>
						option value="">5</option>
					</select>
				</div>
			</div>

			<!-- <div class="timepick">
				<span>Time</span>
				<div class="select-style">
					<select name="" id="">						
						<option value="">10.00 - 12.00 WIB</option>
						<option value="">12.00 - 14.00 WIB</option>
						<option value="">14.00 - 16.00 WIB</option>
						<option value="">16.00 - 18.00 WIB</option>
					</select>
				</div>
			</div> -->

			<!-- <div class="price">
				<span>Harga</span>
				<div class="sum">
					Rp 200.000
					<sub>/Tiket</sub>
				</div>
			</div> -->
		</div>

		<!-- <a href="#" class="booking_btn">
			Booking Now <img src="images/right-arrow.svg" alt="">
		</a> -->
		<input type="submit" class="booking_btn" value="Book Now">		
	</div>
	</form>
</section>


<section class="home-whats-on">
	<div class="container">
		<h1>What's on Trans Snow World</h1>
		<!-- <ul class="whats-on-list alt-list no-btn">
		<li>
		<a href="#">
			<img src="images/news1.jpg" alt="">
			<div class="info">
				<h2>Main Salju di Bekasi, Kapan Saja!</h2>
				<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</p>
				
			</div>
			</a>
		</li>
		<li>
			<a href="#">
			<div class="image-container">
				<img src="images/news2.jpg" alt="">
			</div>
			<div class="info">
				<h2>Main Salju di Bekasi, Kapan Saja! Main Salju di Bekasi, Kapan Saja! Main</h2>					
			</div>
			</a>
		</li>
		<li>
			<a href="#">
			<div class="image-container">
				<img src="images/news3.jpg" alt="">
			</div>
			<div class="info">
				<h2>Main Salju di Bekasi, Kapan Saja!</h2>					
			</div>
			</a>
		</li>
		</ul> -->	
		<div class="whats-on-block">
			<div class="whats-on-content">
				<a href="#">
					<img src="images/news1.jpg" alt="">
					<div class="info">
						<div class="info-content">
							<h2>Main Salju di Bekasi, Kapan Saja!</h2>
							<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</p>
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="whats-on-block">
			<div class="whats-on-content">
				<a href="#">
					<div class="info">
						<div class="info-content">
							<h2>Main Salju di Bekasi, Kapan Saja!</h2>
							<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</p>
						</div>
					</div>
					<img src="images/news2.jpg" alt="">
				</a>
			</div>
		</div>	
		<div class="whats-on-block">
			<div class="whats-on-content">
				<a href="#">
					<img src="images/news3.jpg" alt="">
					<div class="info">
						<div class="info-content">
							<h2>Main Salju di Bekasi, Kapan Saja!</h2>
							<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</p>
						</div>
					</div>
				</a>
			</div>
		</div>		
	</div>	
</section>



<!-- <section class="home-arena">
	<div class="indoor-title">
		<h2>Arena & Fasilitas</h2>
		<p>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	</div>
	<div class="indoor-map">
		
		<picture>
			<img src="images/fasilitas.jpg" alt="">
		</picture>
		<div class="poi" style="top: 42%; left: 19.5%;">
			<a class="poi-trigger">
				<img src="images/icon-poi.png"
				     srcset="images/icon-poi@2x.png 2x,
				             images/icon-poi@3x.png 3x"
				     class="icon_poi">
			</a>
			<div class="info-poi">
				<div class="ratio-poi">
					<picture>
						<img src="images/gal3.jpg" alt="">
					</picture>
				</div>
				<div class="text">
					<h4>Toboggan Area</h4>
					<p>Duis aute irure dolor in reprehenderit in voluptate velit esse</p>
					<a href="#" class="yellow_btn link">Show More <img src="images/right-arrow.svg" alt=""></a>
				</div>
			</div>
		</div>

		<div class="poi" style="top: 84%;left: 28.5%;">
			<a>
				<img src="images/icon-poi.png"
				     srcset="images/icon-poi@2x.png 2x,
				             images/icon-poi@3x.png 3x"
				     class="icon_poi">
			</a>
			<div class="info-poi">
				<div class="ratio-poi">
					<picture>
						<img src="images/gal3.jpg" alt="">
					</picture>
				</div>
				<div class="text">
					<h4>Toboggan Area</h4>
					<p>Duis aute irure dolor in reprehenderit in voluptate velit esse</p>
					<a href="#" class="yellow_btn link">Show More <img src="images/right-arrow.svg" alt=""></a>
				</div>
			</div>
		</div>

		<div class="poi" style="top: 56%;left: 52%;">
			<a>
				<img src="images/icon-poi.png"
				     srcset="images/icon-poi@2x.png 2x,
				             images/icon-poi@3x.png 3x"
				     class="icon_poi">
			</a>
			<div class="info-poi">
				<div class="ratio-poi">
					<picture>
						<img src="images/gal3.jpg" alt="">
					</picture>
				</div>
				<div class="text">
					<h4>Toboggan Area</h4>
					<p>Duis aute irure dolor in reprehenderit in voluptate velit esse</p>
					<a href="#" class="yellow_btn link">Show More <img src="images/right-arrow.svg" alt=""></a>
				</div>
			</div>
		</div>

		<div class="poi" style="top: 33%;left: 55%;">
			<a>
				<img src="images/icon-poi.png"
				     srcset="images/icon-poi@2x.png 2x,
				             images/icon-poi@3x.png 3x"
				     class="icon_poi">
			</a>
			<div class="info-poi">
				<div class="ratio-poi">
					<picture>
						<img src="images/gal3.jpg" alt="">
					</picture>
				</div>
				<div class="text">
					<h4>Toboggan Area</h4>
					<p>Duis aute irure dolor in reprehenderit in voluptate velit esse</p>
					<a href="#" class="yellow_btn link">Show More <img src="images/right-arrow.svg" alt=""></a>
				</div>
			</div>
		</div>

		<div class="poi" style="top: 57%;left: 71.5%;">
			<a>
				<img src="images/icon-poi.png"
				     srcset="images/icon-poi@2x.png 2x,
				             images/icon-poi@3x.png 3x"
				     class="icon_poi">
			</a>
			<div class="info-poi">
				<div class="ratio-poi">
					<picture>
						<img src="images/gal3.jpg" alt="">
					</picture>
				</div>
				<div class="text">
					<h4>Toboggan Area</h4>
					<p>Duis aute irure dolor in reprehenderit in voluptate velit esse</p>
					<a href="#" class="yellow_btn link">Show More <img src="images/right-arrow.svg" alt=""></a>
				</div>
			</div>
		</div>
	</div>	
</section> -->

<!-- <section class="home-fasilitas">
	<div class="container">
		<h3>Fasilitas</h3>
		<ul class="list-fasilitas">
			<li><img src="images/icon-ticket.png" alt="Tiket">Online Ticketing</li>
			<li><img src="images/icon-locker.png" alt="Locker">Locker Room</li>
			<li><img src="images/icon-shoes.png" alt="">Socks &amp; Snow Shoes</li>
			<li><img src="images/icon-nursery.png" alt="">Nursery Room</li>
			<li><img src="images/icon-toilet.png" alt="">Toilet</li>
			<li><img src="images/icon_merchandise.png" alt="">Merchandise Store</li>
		</ul>
	</div>
</section> -->
<?php include "includes/fasilitas.php";?>

<section class="home-others">
	<div class="container">
		<h3>Trans Studio Lainnya</h3>
		<ul class="list-others-trans-studio">
			<li class="studio-cibubur"><a href="#">
				<picture>
					<img src="images/studio-cibubur.jpg" alt="Trans Studio Cibubur">
				</picture>
			</a></li>		
			<li class="studio-bali"><a href="#">
				<picture>
					<img src="images/studio-bali.jpg" alt="">
				</picture>
			</a></li>
			<li class="studio-bandung"><a href="#">
				<picture>
					<img src="images/studio-bandung.jpg" alt="">
				</picture>
			</a></li>
			<li class="studio-bintaro"><a href="#">
				<picture>
					<img src="images/img-snow-1.jpg" alt="">
				</picture>
			</a></li>
		</ul>
	</div>
</section>

<section class="home-gallery">
	<div class="container">
		<h3>Galeri</h3>
		<ul class="list-galeri">
			<li><a href="#">
				<picture>
					<img src="images/gal1.jpg" alt="">
				</picture>
			</a></li>
			<li><a href="#">
				<picture>
					<img src="images/gal2.jpg" alt="">
				</picture>
			</a></li>
			<li><a href="#">
				<picture>
					<img src="images/gal3.jpg" alt="">
				</picture>
			</a></li>
			<li><a href="#">
				<picture>
					<img src="images/gal4.jpg" alt="">
				</picture>
			</a></li>
			<li><a href="#">
				<picture>
					<img src="images/gal5.jpg" alt="">
				</picture>
			</a></li>
			<li><a href="#">
				<picture>
					<img src="images/gal2.jpg" alt="">
				</picture>
			</a></li>
			<li><a href="#">
				<picture>
					<img src="images/gal3.jpg" alt="">
				</picture>
			</a></li>
			<li><a href="#">
				<picture>
					<img src="images/gal4.jpg" alt="">
				</picture>
			</a></li>
			<li><a href="#">
				<picture>
					<img src="images/gal5.jpg" alt="">
				</picture>
			</a></li>
		</ul>
		<!-- <a href=" https://www.instagram.com/transsnowworld.juanda/" target="_blank" class="visit_ig">Follow Instagram @transsnowworld.juanda</a> -->
	</div>
</section>

<!-- <section class="home-address" id="kontakkami">
	<div class="gmap">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.1092520463894!2d107.01483831594363!3d-6.249332195476492!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698fc2abf73209%3A0x8c14db6694414b1e!2sTrans+Snow+World!5e0!3m2!1sen!2sid!4v1554716371565!5m2!1sen!2sid" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	<div class="info">
		<img src="images/logo-tsw.png"
			srcset="
			images/logo-tsw.png 1x,
			images/logo-tsw@2x.png 2x,
			"  alt="Logo Trans Snow World"
			class="logo">
		<address>
			<p>Jln Ir. H.Juanda No 180 Margahayu, Bekasi , Kota Bekasi, Jawa Barat 17113</p>
			<p><a href="tel:+622182692727">(021) 82692727</a></p>
			<p><a href="mailto:contact@transsnowworld.com">contact@transsnowworld.com</a></p>
		</address>
	</div>
</section> -->

<section class="home-insta">
	<div class="container">
		<div class="home-insta__title">Follow us on <img src="images/sm_ig.png" alt=""></div>
		<a href="#" class="yellow_btn">@transsnowworld.juanda</a>
		<a href="#" class="yellow_btn">@transsnowworld.bintaro</a>		
	</div>
</section>

<?php include "includes/footer.php";?>


		  
		  <!-- <div class="text-center">
		  	<a href="#" rel="modal:close" class="close_btn">Setuju</a>
		  </div> -->
</div>


<script type="text/javascript" src="js/slick.min.js"></script>
<script>	
	$('.list_hero').slick({		
		dots: true,
		autoplay: true,
  		autoplaySpeed: 2000,
	});	 
</script>
<?php include "includes/add_on.php";?>

</body>
</html>