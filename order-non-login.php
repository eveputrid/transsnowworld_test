<html lang="en">
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="container container-form">
	<div class="order-step">
		<!-- <div class=""><span>#1</span> CHOOSE PRODUCT TICKET</div> -->
		<div class="selected"><span>#1</span> PESAN TIKET</div>
		<div class=""><span>#2</span> BAYAR</div>
		<div class=""><span>#3</span> TIKET</div>
	</div>
</div>
<div class="container container-form">	
	<div class="data-tiket">
		<div class="title-box">
			Data Tiket
		</div>
		<div class="produk">
			<span>Type of Ticket</span>
			<select name="" id="">
				<option value="">Entry Ticket</option>
				<option value="">CT Corp Employee</option>
				<option value="">CC Mega 25%</option>
				<option value="">CC Mega 50%</option>
			</select>
		</div>
		<div class="datepick">
			<span>Tanggal</span>
			<input id="date-tiket" type="text" placeholder="DD-MM-YYYY">
		</div>	

<!-- 		<div class="timepick">
			<span>Jam Bermain</span>			
				<select name="" id="">						
					<option value="">10.00 - 12.00 WIB</option>
					<option value="">12.00 - 14.00 WIB</option>
					<option value="">14.00 - 16.00 WIB</option>
					<option value="">16.00 - 18.00 WIB</option>
				</select>			
		</div>
 -->
		<div class="qty">
			<span>Jumlah Tiket</span>
			<select name="" id="">
				<option value="">1</option>
				<option value="">2</option>
				<option value="">3</option>
				<option value="">4</option>
			</select>
		</div>

		<div class="price">
			<span>Total</span>
			<div class="sum">
				Rp 100.000				
			</div>
		</div>

		<div class="total">
			<span>TOTAL</span>
			<span>Rp 400.000</span>
		</div>
	</div>

	<div class="box-daftar">
		<img src="images/icon-lock.svg" alt=""><a href="#">Login</a> atau <a href="#">Daftar</a> dengan <strong>detikID</strong> untuk mendapatkan semua akses ke detikNetwork.
	</div>

	<!-- <div class="data-additional">
		<div class="title-box">
			Additional Service
		</div>		

		<div class="group-add">
			<div class="additional">
				<input type="checkbox" id="fasttrack"> <label for="fasttrack">Fast Track <span>@ Rp 100.000,-</span></label>
			</div>

			<div class="qty additional2">
				<span>Buy Service</span>
				<select name="" id="">
					<option value="">1</option>
					<option value="">2</option>
					<option value="">3</option>
					<option value="">4</option>
				</select>
			</div>

			<div class="price additional3">
				<span>Price</span>
				<div class="sum">
					Rp 100.000				
				</div>
			</div>
		</div>

		<div class="group-add">
			<div class="additional">
				<input type="checkbox" id="add1"> <label for="add1">Add 1 <span>@ Rp 100.000,-</span></label>
			</div>

			<div class="qty additional2">
				<span>Buy Service</span>
				<select name="" id="">
					<option value="">1</option>
					<option value="">2</option>
					<option value="">3</option>
					<option value="">4</option>
				</select>
			</div>

			<div class="price additional3">
				<span>Price</span>
				<div class="sum">
					Rp 100.000				
				</div>
			</div>
		</div>

		<div class="group-add">
			<div class="additional">
				<input type="checkbox" id="add2"> <label for="add2">Add 2 <span>@ Rp 100.000,-</span></label>
			</div>

			<div class="qty additional2">
				<span>Buy Service</span>
				<select name="" id="">
					<option value="">1</option>
					<option value="">2</option>
					<option value="">3</option>
					<option value="">4</option>
				</select>
			</div>

			<div class="price additional3">
				<span>Price</span>
				<div class="sum">
					Rp 100.000				
				</div>
			</div>
		</div>

		<div class="total">
			<span>GRAND TOTAL</span>
			<span>Rp 400.000</span>
		</div>

		
	</div> -->
<!-- 
	<div class="notice">
		<strong>Perhatian</strong>
		<ul>
			<li>Entry Ticket digunakan ketika anda akan emmasuki Trans Studio Cibubur.</li>
			<li>Fast Track adalah layanan bebas antri untuk menaiki wahana.</li>
			<li>Fast Track hanya bisa digunakan oleh pengunjung yang telah memiliki entry Tiket.</li>
		</ul>
	</div> -->

	<div class="data-user">
		<div class="title-box">
			Data Pemesan
		</div>
		<form action="notif.php">
			<div class="fullname">
				<span>Nama Lengkap</span>
				<input type="text">
			</div>

			<div class="email">
				<span>Email</span>
				<input type="email">
			</div>

			<div class="phone">
				<span>Nomor Handphone</span>
				<input type="number">
				<a href="#">Verifikasi</a>
				<span class="info">*Silahkan lakukan verifikasi jika ingin mengirimkan informasi pesanan melalui sms</span>
			</div>

			<div class="edit-phone">
				<span>Nomor Handphone</span>
				<span class="view-phone">08918723892</span>
				<a href="#">Ubah</a>
			</div>

			<div class="otp-code">
				<span class="title">Kode OTP</span>
				<input type="text" maxlength="4">
				<span class="countdown">02:44</span>
				<span class="info">*Masukkan kode yang telah kami kirimkan melalui SMS</span>
			</div>

			<div class="ic-type">
				<span>Tipe Identitas</span>
				<select name="" id="">
					<option value="">KTP</option>
					<option value="">SIM</option>					
				</select>
			</div>

			<div class="ic-number">
				<span>Nomor Identitas</span>
				<input type="text" value="1231231241234">
			</div>

			<div class="terms">
				<input id="check-terms" type="checkbox">
				<label for="check-terms"></label>
				<a href="#terms" rel="modal:open">Saya Menyetujui syarat dan ketentuan yang berlaku</a>
			</div>
			<div class="submit">
				<!-- <input type="submit" value="Bayar"> -->
				<a href="#order-summary" rel="modal:open">Bayar</a>
			</div>
		</form>
	</div>
</div>

<div id="terms" class="modal">
	<div class="modal-content">
		<h4>Syarat dan Ketentuan</h4>
		  <p><strong>Title title title</strong><br>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<p><strong>Title title title</strong><br>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<p><strong>Title title title</strong><br>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<p><strong>Title title title</strong><br>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		  <div class="text-center">
		  	<a href="#" rel="modal:close" class="close_btn">Setuju</a>
		  </div>
	</div>
</div>

<div id="order-summary" class="modal">
	<div class="title">
		Order Summary
	</div>
	<div class="subtitle">
		Anda akan membeli tiket <span>Trans Snow World Juanda</span>
	</div>
	<div class="order-confirmation">
		<div class="order-id">
			Putrima - Order ID <span>#1234567890</span>
		</div>
		<span>Pemesanan tiket untuk</span>
		<div>Tanggal</div><div class="content">8 November 2019</div>
		<div>Waktu</div><div class="content">19:00-2100</div>
		<div>Tiket <span class="gofull">Regular (Weekday)</span></div><div class="content">4 Tiket</div>
		<div class="total-billing">
			@ Rp 200.000,- x 4 Tiket
		</div>
		<div class="total-billing payment">
			Total <span>Rp.800.000</span>
		</div>
		<div class="btn-group">
			<a href="#" rel="modal:close">Cancel</a>
			<input type="submit" value="Pembayaran">
		</div>
	</div>
</div>

<?php include "includes/footer.php";?>
<!-- <script>
	var countChecked = function () {
    var n = $("#fasttrack:checked").length;
    if (n >= 1) {
        $(".additional2, .additional3").css({
            "display": "block"
        });
    } else {
        $(".additional2, .additional3").css({
            "display": "none"
        });
    }
};
countChecked();

$("#fasttrack").on("click", countChecked);
</script> -->
<?php include "includes/add_on.php";?>

</body>
</html>