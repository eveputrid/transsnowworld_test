<html lang="en">
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="container">
	<div class="title_page"><h1>Cara Bayar</h1></div>

	<div class="detail-bantuan">
		<ul class="list-bantuan">
			<li class=""><a href="bantuan.php">Cara Pemesanan</a></li>
			<li class="selected"><a href="#">Cara Pembayaran</a></li>
			<li class=""><a href="#">Purna Jual</a></li>
			<li class=""><a href="#">Arena dan Fasilitas</a></li>
		</ul>

		<div class="content-bantuan">
			<h2>STEP 1</h2>			
			<p>Pilih metode pembayaran</p>
			<form class="metode_ul">
				<input type="radio" name=""><label>BCA</label>
				<input type="radio" name=""><label>MEGA</label>
				<input type="radio" name=""><label>MANDIRI</label>
				<input type="radio" name=""><label>BNI</label>
			</form>
			<h2>STEP 2</h2>			
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
			<img src="images/news1.jpg" alt="">
			<h2>Title title title</h2>			
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

			<ol>
				<li>List item 1</li>
				<li>List item 2</li>
				<li>List item 3</li>
				<li>List item 4</li>
				<li>List item 5</li>
			</ol>

			<h2>STEP 3</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

			<table>
				<tr>
					<td>konten 1</td>
					<td>Konten 2</td>
				</tr>
				<tr>
					<td>konten 3</td>
					<td>Konten 4</td>
				</tr>
			</table>

			<h2>STEP 4</h2>			
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>

			<h2>STEP 5</h2>			
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
	</div>
</div>
<?php include "includes/footer.php";?>
</body>
</html>